<?php

/**
 * @file
 * Contains Drupal\quick_pages\QuickPageInterface.
 */

namespace Drupal\quick_pages;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a quick page entity type.
 */
interface QuickPageInterface extends ConfigEntityInterface {

}
